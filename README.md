# README #

This project was made as part of a challenge and implements a simple REST API over dotnet core 5.0  
and Entity Framework 6.0.

As a database I chose to use Entity Framework InMemory feature which means all the data exists only  
in memory and lives as long as the solution is running.

No concurrency/transactions/relative database operations are supported.  
The Patch requests are made using JsonPatch. Most JsonSerialization is done via System.Text.Json  
except JsonPatch requires Newtonsoft's Json serialization - solution uses both when necesary.  

All of the tests are executed over Microsoft.AspNetCore.TestServer with the Api's Startup configuration.  
This and having all tests run as async makes testing go faster and run "locally" without needing and instance running.  

Test are written assuming the "database" has no changes since creation.  
All pass when ran through Visual Studio or with the script file "test.ps1"

This project includes 3 script files:  
- build.ps1 - it builds  
- run.ps1 - it runs  
- test.ps1 - it tests  

### How do I get set up? ###

This project needs dotnet core 5.0 installed.  
There is a final tag in the repository.  

Use any of the above mentioned scripts to build, test and run.  

### Who do I talk to? ###

* h1afilhado@gmail.com
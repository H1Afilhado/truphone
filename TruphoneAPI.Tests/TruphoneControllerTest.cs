using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using TruphoneAPI.Models;
using Xunit;
using Xunit.Abstractions;

namespace TruphoneAPI.Tests
{
    public class TruphoneControllerTest
    {
        private TestServer _server;
        private HttpClient _client;
        private readonly GenericDevice[] _genericDevices = new GenericDevice[] {
            new GenericDevice() { Id = Guid.Parse("489c40a8-6d63-40e8-b31b-49d39fe686a3"), Name = "Dev001", Brand = "Brand 001", CreationDate = DateTime.Parse("01-06-2021") },
            new GenericDevice() { Id = Guid.Parse("49c2382d-e7fe-4bce-9237-2911853a713b"), Name = "Dev002", Brand = "Brand 002", CreationDate = DateTime.Parse("02-06-2021") },
            new GenericDevice() { Id = Guid.Parse("a06759b3-9631-455f-8b4f-04854c49128f"), Name = "Dev003", Brand = "Brand 002", CreationDate = DateTime.Parse("02-06-2021") },
            new GenericDevice() { Id = Guid.Parse("2c6d046a-9c40-42e7-9c77-d619e733d74e"), Name = "Dev004", Brand = "Brand 003", CreationDate = DateTime.Parse("03-06-2021") },
            new GenericDevice() { Id = Guid.Parse("cb371314-a2d8-4032-8406-1190f009d7d0"), Name = "Dev005", Brand = "Brand 003", CreationDate = DateTime.Parse("03-06-2021") },
            new GenericDevice() { Id = Guid.Parse("c9a5d50e-f7f9-431f-b76d-278d3d067018"), Name = "Dev006", Brand = "Brand 003", CreationDate = DateTime.Parse("03-06-2021") },
            new GenericDevice() { Id = Guid.Parse("08a0411c-db0d-40f6-959a-5070f4cc8d2d"), Name = "Dev007", Brand = "Brand 004", CreationDate = DateTime.Parse("04-06-2021") },
            new GenericDevice() { Id = Guid.Parse("f3053e17-363b-4b5f-91b8-e40b7ea065b0"), Name = "Dev008", Brand = "Brand 004", CreationDate = DateTime.Parse("04-06-2021") },
            new GenericDevice() { Id = Guid.Parse("7c00ffcb-74bd-4583-85cc-dc62cb215acb"), Name = "Dev009", Brand = "Brand 004", CreationDate = DateTime.Parse("04-06-2021") },
            new GenericDevice() { Id = Guid.Parse("d890bc05-93ba-4458-9d73-4bfa0d145489"), Name = "Dev010", Brand = "Brand 004", CreationDate = DateTime.Parse("04-06-2021") } };

        public TruphoneControllerTest()
        {

            _server = new TestServer(new WebHostBuilder()
                                        .UseStartup<Startup>());
            _client = _server.CreateClient();

        }
        private DeviceContext _context => new DeviceContext(new DbContextOptionsBuilder<DeviceContext>().UseInMemoryDatabase("TruphoneDevices").Options);

        [Fact]
        public async Task GetRightDeviceByGuid()
        {
            var response = await _client.GetAsync($"/truphone/{_genericDevices[0].Id}");
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.OK);

            var responseString = await response.Content.ReadAsStringAsync();
            var device = JsonSerializer.Deserialize<GenericDevice>(responseString);
            Assert.True(device.Id.Equals(_genericDevices[0].Id));

            response = await _client.GetAsync($"/truphone/{_genericDevices[3].Id}");
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.OK);

            responseString = await response.Content.ReadAsStringAsync();
            device = JsonSerializer.Deserialize<GenericDevice>(responseString);
            Assert.True(device.Id.Equals(_genericDevices[3].Id));
        }

        [Fact]
        public async Task GetOkReturnedAndNullResponseOnNonExistingGuid()
        {
            var guid = "489c40a8-6d63-40e8-b31b-49d39fe686a4";
            var response = await _client.GetAsync($"/truphone/{guid}");
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task ListAllDevicesInDatabase()
        {
            var response = await _client.GetAsync("/truphone/list");
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.OK);

            var responseString = await response.Content.ReadAsStringAsync();
            var returnedDeviceList = JsonSerializer.Deserialize<IEnumerable<GenericDevice>>(responseString);
            Assert.All(returnedDeviceList, device => Assert.Contains(device.Id, _genericDevices.Select(x => x.Id)));
            Assert.All(_genericDevices, device => Assert.Contains(device.Id, returnedDeviceList.Select(x => x.Id)));
        }

        [Fact]
        public async Task CreatedDeviceSuccessfullyWithoutGuidInRequest()
        {
            var newDevice = new GenericDevice() { Name = "Dev X", Brand = "Brand Y", CreationDate = DateTime.Now };
            var content = new StringContent(JsonSerializer.Serialize(newDevice), System.Text.Encoding.Default, "application/json");
            var response = await _client.PostAsync("/truphone/create", content);
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.Created);

            var responseString = await response.Content.ReadAsStringAsync();
            var createdDeviceGuid = JsonSerializer.Deserialize<Guid>(responseString);
            newDevice.Id = createdDeviceGuid;

            response = await _client.GetAsync($"/truphone/{createdDeviceGuid}");
            responseString = await response.Content.ReadAsStringAsync();

            var returnedDevice = JsonSerializer.Deserialize<GenericDevice>(responseString);
            Assert.True(newDevice.Equals(returnedDevice));


        }

        [Fact]
        public async Task CreateDeviceSuccessfullyWithRandomGuidInRequest()
        {
            var newDeviceGuid = Guid.NewGuid();
            var newDevice = new GenericDevice() { Id = newDeviceGuid, Name = "Dev X", Brand = "Brand Y", CreationDate = DateTime.Now };
            var content = new StringContent(JsonSerializer.Serialize(newDevice), System.Text.Encoding.Default, "application/json");

            var response = await _client.PostAsync("/truphone/create", content);
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.Created);

            var responseString = await response.Content.ReadAsStringAsync();
            var createdDeviceGuid = JsonSerializer.Deserialize<Guid>(responseString);
            newDevice.Id = createdDeviceGuid;

            response = await _client.GetAsync($"/truphone/{createdDeviceGuid}");
            responseString = await response.Content.ReadAsStringAsync();

            var returnedDevice = JsonSerializer.Deserialize<GenericDevice>(responseString);
            Assert.True(newDevice.Equals(returnedDevice));


        }

        [Fact]
        public async Task CreateReturnsUnprocessableEntityOnNullOrWhitespaceDevice()
        {
            var content = new StringContent(JsonSerializer.Serialize("null"), System.Text.Encoding.Default, "application/json");
            var response = await _client.PostAsync("/truphone/create", content);
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.BadRequest);

            content = new StringContent(JsonSerializer.Serialize("  "), System.Text.Encoding.Default, "application/json");
            response = await _client.PostAsync("/truphone/create", content);
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task CreateReturnsUnprocessableEntityOnNullOrWhitespaceDeviceProperties()
        {
            var newDevice = new GenericDevice();
            var content = new StringContent(JsonSerializer.Serialize(newDevice), System.Text.Encoding.Default, "application/json");
            var response = await _client.PostAsync("/truphone/create", content);
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.UnprocessableEntity);

            newDevice = new GenericDevice() { Name = "  ", Brand = "  " };
            content = new StringContent(JsonSerializer.Serialize(newDevice), System.Text.Encoding.Default, "application/json");
            response = await _client.PostAsync("/truphone/create", content);
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.UnprocessableEntity);
        }

        [Fact]
        public async Task UpdateFullDeviceWithSuccess()
        {
            var deviceUpdate = _genericDevices[3].Clone();
            deviceUpdate.Name = "Updated Name X";
            deviceUpdate.Brand = "Updated Brand X";
            var content = new StringContent(JsonSerializer.Serialize(deviceUpdate), System.Text.Encoding.Default, "application/json");
            var response = await _client.PutAsync($"/truphone/update/{deviceUpdate.Id}", content);
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.NoContent);

            response = await _client.GetAsync($"/truphone/{deviceUpdate.Id}");
            var responseString = await response.Content.ReadAsStringAsync();

            var returnedDevice = JsonSerializer.Deserialize<GenericDevice>(responseString);
            Assert.True(returnedDevice.Equals(deviceUpdate));
        }

        [Fact]
        public async Task UpdateReturnsNotFoundOnDeviceWithNonExistingGuid()
        {
            var deviceUpdate = _genericDevices[4].Clone();
            deviceUpdate.Name = "Updated Name Y";
            deviceUpdate.Brand = "Updated Brand Y";
            deviceUpdate.Id = new Guid();
            var content = new StringContent(JsonSerializer.Serialize(deviceUpdate), System.Text.Encoding.Default, "application/json");
            var response = await _client.PutAsync($"/truphone/update/{Guid.NewGuid()}", content);
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task PatchDeviceReplaceNameWithValidValue()
        {
            var newDeviceName = "New Device Name";
            var updatedDevice = _genericDevices[4];
            var patchDocument = new JsonPatchDocument<GenericDevice>();

            patchDocument.Replace(dev => dev.Name, newDeviceName);

            var payload = Newtonsoft.Json.JsonConvert.SerializeObject(patchDocument);
            var content = new StringContent(payload, System.Text.Encoding.Default, "application/json-patch+json");
            var response = await _client.PatchAsync($"/truphone/patch/{updatedDevice.Id}", content);
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.NoContent);

            response = await _client.GetAsync($"/truphone/{updatedDevice.Id}");
            var responseString = await response.Content.ReadAsStringAsync();

            var returnedDevice = JsonSerializer.Deserialize<GenericDevice>(responseString);
            Assert.True(returnedDevice.Name.Equals(newDeviceName));


        }

        [Fact]
        public async Task PatchDeviceReplaceBrandWithValidValue()
        {
            var newDeviceBrand = "New Brand Name";
            var updatedDevice = _genericDevices[3];
            var patchDocument = new JsonPatchDocument<GenericDevice>();

            patchDocument.Replace(dev => dev.Brand, newDeviceBrand);

            var payload = Newtonsoft.Json.JsonConvert.SerializeObject(patchDocument);
            var content = new StringContent(payload, System.Text.Encoding.Default, "application/json-patch+json");
            var response = await _client.PatchAsync($"/truphone/patch/{updatedDevice.Id}", content);
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.NoContent);

            response = await _client.GetAsync($"/truphone/{updatedDevice.Id}");
            var responseString = await response.Content.ReadAsStringAsync();

            var returnedDevice = JsonSerializer.Deserialize<GenericDevice>(responseString);
            Assert.True(returnedDevice.Brand.Equals(newDeviceBrand));


        }

        [Fact]
        public async Task PatchDeviceReplaceCreationDateWithValidValue()
        {
            var newDeviceCreationDate = DateTime.Now;
            var updatedDevice = _genericDevices[3];
            var patchDocument = new JsonPatchDocument<GenericDevice>();

            patchDocument.Replace(dev => dev.CreationDate, newDeviceCreationDate);

            var payload = Newtonsoft.Json.JsonConvert.SerializeObject(patchDocument);
            var content = new StringContent(payload, System.Text.Encoding.Default, "application/json-patch+json");
            var response = await _client.PatchAsync($"/truphone/patch/{updatedDevice.Id}", content);
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.NoContent);

            response = await _client.GetAsync($"/truphone/{updatedDevice.Id}");
            var responseString = await response.Content.ReadAsStringAsync();

            var returnedDevice = JsonSerializer.Deserialize<GenericDevice>(responseString);
            Assert.True(returnedDevice.CreationDate.Equals(newDeviceCreationDate));


        }

        [Fact]
        public async Task PatchReturnsUnprocessableEntityOnGuidFieldPatch()
        {
            var newDeviceGuid = Guid.NewGuid();
            var updatedDevice = _genericDevices[2];
            var patchDocument = new JsonPatchDocument<GenericDevice>();

            patchDocument.Replace(dev => dev.Id, newDeviceGuid);

            var payload = Newtonsoft.Json.JsonConvert.SerializeObject(patchDocument);
            var content = new StringContent(payload, System.Text.Encoding.Default, "application/json-patch+json");
            var response = await _client.PatchAsync($"/truphone/patch/{updatedDevice.Id}", content);
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.UnprocessableEntity);


        }

        [Fact]
        public async Task DeleteReturnsOkOnExistingGuid()
        {
            var response = await _client.DeleteAsync($"/truphone/delete/{_genericDevices[1].Id}");
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.OK);

            response = await _client.GetAsync($"/truphone/{_genericDevices[1].Id}");
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task SearchDeviceByBrandName()
        {
            var deviceBrand = _genericDevices[0].Brand;
            var response = await _client.GetAsync($"/truphone/search?brand={Uri.EscapeUriString(deviceBrand)}");
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.OK);

            var responseString = await response.Content.ReadAsStringAsync();
            var returnedDeviceList = JsonSerializer.Deserialize<IEnumerable<GenericDevice>>(responseString);
            Assert.True(returnedDeviceList.Count() == 1);
        }

        [Fact]
        public async Task SearchDeviceByPartialBrandName()
        {
            var deviceBrand = "4";
            var response = await _client.GetAsync($"/truphone/search?brand={Uri.EscapeUriString(deviceBrand)}");
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.OK);

            var responseString = await response.Content.ReadAsStringAsync();
            var returnedDeviceList = JsonSerializer.Deserialize<IEnumerable<GenericDevice>>(responseString);
            Assert.True(returnedDeviceList.Count() == 4);
            Assert.All(returnedDeviceList, device => device.Brand.Contains(deviceBrand));
        }

        [Fact]
        public async Task SearchDeviceByNonExistingBrandName()
        {
            var deviceBrand = "Non Brand";
            var response = await _client.GetAsync($"/truphone/search?brand={Uri.EscapeUriString(deviceBrand)}");
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.OK);

            var responseString = await response.Content.ReadAsStringAsync();
            var returnedDeviceList = JsonSerializer.Deserialize<IEnumerable<GenericDevice>>(responseString);
            Assert.True(returnedDeviceList.Count() == 0);

            deviceBrand = "Another Non Brand";
            response = await _client.GetAsync($"/truphone/search?brand={Uri.EscapeUriString(deviceBrand)}");
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.OK);

            responseString = await response.Content.ReadAsStringAsync();
            returnedDeviceList = JsonSerializer.Deserialize<IEnumerable<GenericDevice>>(responseString);
            Assert.True(returnedDeviceList.Count() == 0);
        }
    }
}

using System;
using Microsoft.EntityFrameworkCore;

namespace TruphoneAPI.Models
{
    public class DeviceContext : DbContext
    {
        public DbSet<GenericDevice> Devices { get; set; }
        public DeviceContext(DbContextOptions<DeviceContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GenericDevice>().HasData(new GenericDevice() { Id = Guid.Parse("489c40a8-6d63-40e8-b31b-49d39fe686a3"), Name = "Dev001", Brand = "Brand 001", CreationDate = DateTime.Parse("01-06-2021") });
            modelBuilder.Entity<GenericDevice>().HasData(new GenericDevice() { Id = Guid.Parse("49c2382d-e7fe-4bce-9237-2911853a713b"), Name = "Dev002", Brand = "Brand 002", CreationDate = DateTime.Parse("02-06-2021") });
            modelBuilder.Entity<GenericDevice>().HasData(new GenericDevice() { Id = Guid.Parse("a06759b3-9631-455f-8b4f-04854c49128f"), Name = "Dev003", Brand = "Brand 002", CreationDate = DateTime.Parse("02-06-2021") });
            modelBuilder.Entity<GenericDevice>().HasData(new GenericDevice() { Id = Guid.Parse("2c6d046a-9c40-42e7-9c77-d619e733d74e"), Name = "Dev004", Brand = "Brand 003", CreationDate = DateTime.Parse("03-06-2021") });
            modelBuilder.Entity<GenericDevice>().HasData(new GenericDevice() { Id = Guid.Parse("cb371314-a2d8-4032-8406-1190f009d7d0"), Name = "Dev005", Brand = "Brand 003", CreationDate = DateTime.Parse("03-06-2021") });
            modelBuilder.Entity<GenericDevice>().HasData(new GenericDevice() { Id = Guid.Parse("c9a5d50e-f7f9-431f-b76d-278d3d067018"), Name = "Dev006", Brand = "Brand 003", CreationDate = DateTime.Parse("03-06-2021") });
            modelBuilder.Entity<GenericDevice>().HasData(new GenericDevice() { Id = Guid.Parse("08a0411c-db0d-40f6-959a-5070f4cc8d2d"), Name = "Dev007", Brand = "Brand 004", CreationDate = DateTime.Parse("04-06-2021") });
            modelBuilder.Entity<GenericDevice>().HasData(new GenericDevice() { Id = Guid.Parse("f3053e17-363b-4b5f-91b8-e40b7ea065b0"), Name = "Dev008", Brand = "Brand 004", CreationDate = DateTime.Parse("04-06-2021") });
            modelBuilder.Entity<GenericDevice>().HasData(new GenericDevice() { Id = Guid.Parse("7c00ffcb-74bd-4583-85cc-dc62cb215acb"), Name = "Dev009", Brand = "Brand 004", CreationDate = DateTime.Parse("04-06-2021") });
            modelBuilder.Entity<GenericDevice>().HasData(new GenericDevice() { Id = Guid.Parse("d890bc05-93ba-4458-9d73-4bfa0d145489"), Name = "Dev010", Brand = "Brand 004", CreationDate = DateTime.Parse("04-06-2021") });
        }
    }
}
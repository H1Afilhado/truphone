﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TruphoneAPI.Models;

namespace TruphoneAPI.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class TruphoneController : ControllerBase
    {
        private readonly ILogger<TruphoneController> _logger;
        private readonly DeviceContext _context;

        public TruphoneController(ILogger<TruphoneController> logger, DeviceContext context)
        {
            _logger = logger;
            _context = context;
        }

        //1
        [HttpPost]
        public ActionResult<string> Create([FromBody] GenericDevice device)
        {
            if (device == null)
                return UnprocessableEntity();
            else if (string.IsNullOrWhiteSpace(device.Name) || string.IsNullOrWhiteSpace(device.Brand))
                return UnprocessableEntity();
            else
            {
                var newDevice = new GenericDevice()
                {
                    Id = Guid.NewGuid(),
                    Name = device.Name,
                    Brand = device.Brand,
                    CreationDate = device.CreationDate
                };
                _context.Devices.Add(newDevice);
                _context.SaveChanges();

                return Created($"/truphone?{newDevice.Id}", JsonSerializer.Serialize(newDevice.Id));
            }
        }
        //2
        [HttpGet]
        [Route("/[controller]/{id:Guid}")]
        public ActionResult<string> Get(Guid id)
        {
            var a = _context.Devices.FirstOrDefault(dev => dev.Id.Equals(id));
            if (a == default)
                return NotFound();
            else
                return Ok(JsonSerializer.Serialize(a));
        }
        //3
        [HttpGet]
        public ActionResult<string> List()
        {
            return Ok(JsonSerializer.Serialize(_context.Devices));
        }
        //4
        [HttpPut("{id:Guid}")]
        public ActionResult<string> Update(Guid id, [FromBody] GenericDevice device)
        {
            if (_context.Devices.Select(dev => dev.Id).Contains(id))
            {
                var deviceToUpdate = _context.Devices.FirstOrDefault(dev => dev.Id.Equals(id));
                if (deviceToUpdate == default)
                    return NotFound();
                else
                {
                    device.Id = id;
                    _context.Devices.Remove(deviceToUpdate);
                    _context.Devices.Add(device);
                    _context.SaveChanges();

                    return NoContent();
                }
            }
            else
                return NotFound();
        }
        //4
        [HttpPatch("{id:Guid}")]
        public ActionResult<string> Patch(Guid id, [FromBody] JsonPatchDocument<GenericDevice> devicePatch)
        {
            if (devicePatch == null || devicePatch.Operations.Count == 0 || devicePatch.Operations.Any(x => x.path.ToLower().Equals("/id")))
                return UnprocessableEntity();
            else if (!_context.Devices.Any(dev => dev.Id.Equals(id)))
                return NotFound();
            else
            {
                var deviceToPatch = _context.Devices.FirstOrDefault(dev => dev.Id.Equals(id));
                devicePatch.ApplyTo(deviceToPatch);

                _context.SaveChanges();

                return NoContent();
            }
        }
        //5
        [HttpDelete("{id:Guid}")]
        public ActionResult<string> Delete(Guid id)
        {
            if (_context.Devices.Select(dev => dev.Id).Contains(id))
            {
                var device = _context.Devices.FirstOrDefault(dev => dev.Id.Equals(id));
                if (device == default)
                    return NotFound();
                else
                {
                    _context.Devices.Remove(device);
                    _context.SaveChanges();

                    return Ok();
                }
            }
            else
                return NotFound();
        }
        //6
        [HttpGet]
        public ActionResult<string> Search(string brand)
        {
            if (string.IsNullOrWhiteSpace(brand))
                return UnprocessableEntity();
            else
            {
                var deviceList = _context.Devices.Where(device => device.Brand.Contains(brand));

                return JsonSerializer.Serialize<IEnumerable<GenericDevice>>(deviceList);
            }
        }
    }
}

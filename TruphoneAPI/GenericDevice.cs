using System;
using System.Text.Json.Serialization;

namespace TruphoneAPI.Models
{
    public class GenericDevice
    {
        // Set accessor could be private. Its public to allow easier testing
        [JsonInclude]
        public Guid Id { get; set; }
        [JsonInclude]
        public DateTime CreationDate { get; set; }
        [JsonInclude]
        public string Name { get; set; }
        [JsonInclude]
        public string Brand { get; set; }

        public override bool Equals(object obj)
        {
            var isOfType = obj.GetType().Equals(typeof(GenericDevice));
            if(!isOfType)
                return false;
            else {
                var device = (GenericDevice)obj;
                return Id.Equals(device.Id) && Name.Equals(device.Name) && Brand.Equals(device.Brand) && CreationDate.Equals(device.CreationDate);
            }
        }

        public GenericDevice Clone() {
            return new GenericDevice() { Id = Id, Name = Name, Brand = Brand, CreationDate = CreationDate };
        }
    }
}
